import 'package:done/home_page.dart';
import 'package:done/localization/demo_localization.dart';
import 'package:done/log_in.dart';
import 'package:done/on_board_screen.dart';
import 'package:done/sign_up_first_screen.dart';
import 'package:done/sign_up_second_screen.dart';
import 'package:done/splash_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
//import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'themes.dart';

void main() => runApp(ChangeNotifierProvider<ThemeModel>(
    create: (BuildContext context) => ThemeModel(), child: MyApp()));

class MyApp extends StatefulWidget {

  static void setLocale(BuildContext context, Locale locale)
  {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(locale);
  }
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  Locale _locale;
  void setLocale(Locale locale){
    setState(() {
      _locale=locale;
    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        /*theme: lightTheme.copyWith(
        textTheme: GoogleFonts.balooTammuduTextTheme(
          Theme.of(context).textTheme,
        )
      ),*/
        debugShowCheckedModeBanner: false,
        theme: Provider.of<ThemeModel>(context).currentTheme,
        initialRoute: '/',
        routes: {
          '/': (context) => SplashScreen(),
          '/LogIn': (context) => LogIn(),
          '/SignUp1': (context) => SignUp1(),
          '/SignUp2': (context) => SignUp2(),
          '/OnBoardScreen': (context) => OnBoardScreen(),
          '/HomePage': (context) => HomePage(),
        },
        locale: _locale,
        supportedLocales:[
         Locale('en','US'),
         Locale('ar','SA')
         ],
        localizationsDelegates: [
          DemoLocalization.delegate,
          GlobalMaterialLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocales) {
          for (var locale in supportedLocales) {
            if (locale.languageCode == locale.languageCode &&
                locale.countryCode == locale.countryCode) {
              return deviceLocale;
            }
          }
          return supportedLocales.first;
        },
        );

    /*initialRoute: '/',
        routes: {
          '/': (context) => SplashScreen(),
          '/first': (context) => LogIn(),
          '/second': (context) => SignUp(),*/
  }
}
