import 'dart:async';
import 'package:done/log_in.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return StartState();
  }
}

class StartState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    startTimer();
  }

  startTimer() async {
    Duration duration = Duration(seconds: 8);
    return Timer(duration, route);
  }

  route() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (context) => LogIn()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFF7F7F7),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Container(
              child: Image.asset('assets/images/logo.jpg'),
            ),
            Padding(padding: EdgeInsets.only(top: 15)),
            Text(
              'GET YOUR TEAM TASK DONE WITH DONE!',
              style: TextStyle(fontSize: 25, color: Colors.red),
            ),
            Padding(padding: EdgeInsets.only(top: 15)),
            CircularProgressIndicator(
              backgroundColor: Colors.white,
              strokeWidth: 1,
            ),
          ],
        ),
      ),
    );
  }
}

/*import 'dart:io';
import 'package:done/log_in.dart';
import 'package:flutter/material.dart';

class SplashScreen extends StatelessWidget {

  void splashScreenTimer() {
    Duration threeSeconds = Duration(seconds: 3);
    sleep(threeSeconds);
  }
  @override
  Widget build(BuildContext context) {
    //When the Splash Screen is Finished Go to Log in Screen
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Image.asset('LOGO.jpg'),
        FutureBuilder(
          future: Future.delayed(Duration(seconds: 3)),
          builder: (c, s) => s.connectionState != ConnectionState.done ? Text('Loading...'),

        )
      ],

    );
  }
}*/
