import 'package:flutter/material.dart';
//import 'package:google_fonts/google_fonts.dart';
import 'main.dart';

//RobotoMono
//BalooTammudu2
ThemeData lightTheme = ThemeData.light().copyWith(
  primaryColor: Color(0xFFF7F7F7), //LIGHT BACKGROUND
  accentColor: Color(0xFFFABB18), //YELLOW
  textTheme: TextTheme(
    title: TextStyle(
      color: Color(0xff000000),
    ),
    subtitle: TextStyle(
      color: Color(0xff000000),
    ),
    subhead: TextStyle(
      color: Color(0xff000000),
    ),
  ),
  /*ThemeData.light().textTheme.apply(
    fontFamily: 'BalooTammudu2',
  ),
  primaryTextTheme: ThemeData.light().textTheme.apply(
    fontFamily: 'BalooTammudu2',
  ),
  accentTextTheme: ThemeData.light().textTheme.apply(
    fontFamily: 'BalooTammudu2',
  ),*/

  /*GoogleFonts.balooTammuduTextTheme(),*/
  appBarTheme: AppBarTheme(
    color: Color(0xFFFABB18),
  ),
); //YELLOW

ThemeData darkTheme = ThemeData.dark().copyWith(
  primaryColor: Color(0xFF51585F),
  accentColor: Color(0xFF000000),
  textTheme: TextTheme(
    title: TextStyle(
      color: Color(0xFFFFFFFF),
    ),
    subtitle: TextStyle(
      color: Color(0xFFFFFFFF),
    ),
    subhead: TextStyle(
      color: Color(0xFFFFFFFF),
    ),
    /*ThemeData.dark().textTheme.apply(
  fontFamily: 'BalooTammudu2',
),
    primaryTextTheme: ThemeData.dark().textTheme.apply(
fontFamily: 'BalooTammudu2',
),
accentTextTheme: ThemeData.dark().textTheme.apply(
fontFamily: 'BalooTammudu2',
),*/
  ),
  appBarTheme: AppBarTheme(
    color: Color(0xFF000000),
  ),
);

enum ThemeType { Light, Dark }

class ThemeModel extends ChangeNotifier {
  ThemeData currentTheme = lightTheme;
  ThemeType _themeType = ThemeType.Light;

  toggleTheme() {
    if (_themeType == ThemeType.Dark) {
      currentTheme = lightTheme;
      _themeType = ThemeType.Light;
      return notifyListeners();
    }

    if (_themeType == ThemeType.Light) {
      currentTheme = darkTheme;
      _themeType = ThemeType.Dark;
      return notifyListeners();
    }
  }
}
