import 'package:flutter/material.dart';

class OnBoardScreen extends StatefulWidget {
  @override
  _OnBoardScreenState createState() => _OnBoardScreenState();
}

class _OnBoardScreenState extends State<OnBoardScreen> {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          child: Text(
            'This is OnBoard Screen!!!',
            style: TextStyle(
              fontSize: 30,
            ),
          ),
        ),
        RaisedButton(
            child: Text('End of OnBoardScreen'),
            onPressed: () {
              Navigator.pushNamed(context, '/HomePage');
            })
      ],
    );
  }
}
