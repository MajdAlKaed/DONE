import 'package:flutter/material.dart';
import 'themes.dart';
import 'package:provider/provider.dart';
import 'sign_up_first_screen.dart';

class LogIn extends StatelessWidget {
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                child: Image.asset('assets/images/logo.jpg', height: 142, width: 142),
              ),
              Text('Sign In', style: TextStyle(color: Colors.black),),
              Text('Hi there! Nice to See You Again.', style: TextStyle(color: Colors.black),),
              Text('Email', style: TextStyle(color: Colors.black),),
              Container(
                width: 280,
                child: TextField(
                  decoration: InputDecoration(
                    labelText: 'Email',
                    hintText: 'example@email.com',
                  ),
                ),
              ),
              SizedBox(height: 25,),
              Text('Password', style: TextStyle(color: Colors.black),),
              Container(
                width: 280,
                child: TextField(
                  decoration: InputDecoration(
                    labelText: 'Password',
                  ),
                ),
              ),
              RaisedButton(
                child: Text('Sign In'),
                onPressed: () {
                  Navigator.pushNamed(context, '/HomePage');
                },
              ),
              SizedBox(
                height: 10,
              ),
              Text('or use one of your social profiles', style: TextStyle(color: Colors.black),),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                      child: Text('Google'),
                      onPressed: () {
                        Navigator.pushNamed(context, '/HomePage');
                      }),
                  SizedBox(
                    width: 5,
                  ),
                  RaisedButton(
                      child: Text('Facebook'),
                      onPressed: () {
                        Navigator.pushNamed(context, '/HomePage');
                      }),
                ],
              ),
              //SizedBox(height: 55,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  RaisedButton(
                    child: Text('Forgot Password?'),
                    onPressed: () {},
                  ),
                  SizedBox(
                    width: 95,
                  ),
                  RaisedButton(
                    child: Text('Sign Up'),
                    onPressed: () {
                      Navigator.pushNamed(context, '/SignUp1');
                    },
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

/*RaisedButton(
child: Text(
'Change Theme',
),
onPressed: () {
Provider.of<ThemeModel>(context, listen: false).toggleTheme();
},
),*/

/*
Drawer(
child: ListView(
children: [
DrawerHeader(),
ListTile(
title: Text('Theme'),
trailing: Switch(
value: false,
onChanged: (changedtheme) {
Provider.of<ThemeModel>(context, listen: false).toggleTheme();
},
),
)
],
),
),*/
