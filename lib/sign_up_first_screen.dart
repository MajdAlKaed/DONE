import 'package:flutter/material.dart';

class SignUp1 extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<SignUp1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,

          children: [
            Text('Sign Up'),
            SizedBox(
              height: 15,
            ),
            Text('Email'),
            TextField(
              decoration: InputDecoration(
                labelText: 'Email',
                hintText: 'example@email.com',
              ),
            ),
            SizedBox(height: 15,),
            Text('Password'),
            TextField(
              decoration: InputDecoration(),
            ),
            SizedBox(height: 15,),
            Text('Phone Number'),
            Row(
              children: [
                Text('+'),
                TextField(
                  decoration: InputDecoration(
                    labelText: 'Country Code',
                  ),
                ),
                SizedBox(width: 5,),
                TextField(
                  decoration: InputDecoration(),
                ),
              ],
            ),
            RaisedButton(
              textColor: Colors.white,
              color: Colors.blue,
              child: Text('Continue'),
              onPressed: () {
                Navigator.pushNamed(context, '/SignUp2');
              },
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Text('Already Have an Account? '),
                RaisedButton(
                  onPressed: () {
                    Navigator.pushNamed(context, '/LogIn');
                  },
                  child: Text('Sign In'),
                ),
              ],
            )
          ],
        ),
      ),
    );
  }
}
