import 'package:flutter/material.dart';

class SignUp2 extends StatefulWidget {
  @override
  _State createState() => _State();
}

class _State extends State<SignUp2> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        //mainAxisAlignment: MainAxisAlignment.,
        children: [
          Text('Sign Up'),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Text('First Name'),
              SizedBox(
                width: 48,
              ),
              Text('Second Name'),
            ],
          ),
          SizedBox(
            height: 10,
          ),
          Row(
            children: [
              TextField(
                decoration: InputDecoration(),
              ),
              SizedBox(
                width: 32,
              ),
              TextField(
                decoration: InputDecoration(),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),
          Text('Verification Code'),
          TextField(
            decoration: InputDecoration(),
          ),
          SizedBox(
            height: 25,
          ),
          RaisedButton(
            textColor: Colors.white,
            color: Colors.blue,
            child: Text('Sign Up'),
            onPressed: () {
              Navigator.pushNamed(context, '/OnBoardScreen');
            },
          ),
          SizedBox(
            height: 15,
          ),
          Row(
            children: [
              Text('Already Have an Account? '),
              RaisedButton(
                onPressed: () {
                  Navigator.pushNamed(context, '/LogIn');
                },
                child: Text('Sign In'),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
