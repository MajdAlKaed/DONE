//FOLLOW THE COMMENTS
//FOLLOW THE COMMENTS
//FOLLOW THE COMMENTS

import 'package:done/classes/language.dart';
import 'package:done/main.dart';
import 'package:done/themes.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'localization/language_constants.dart';

class HomePage extends StatefulWidget {
  @override
  _ToggleButtonsState createState() => _ToggleButtonsState();
}

class _ToggleButtonsState extends State<HomePage> {
  final padding = EdgeInsets.symmetric(horizontal: 20);
  List<bool> isSelected = [true, false];
  @override
  Widget build(BuildContext context) {
    final name = getTranslated(context, 'name');
    final email = getTranslated(context, 'email');
    final urlimg = Image(
      image: AssetImage('assets/images/profile.jpg'),
    );
    return Scaffold(
      appBar: AppBar(
          title: Text(getTranslated(context, 'home_page')),
          actions: <Widget>[
            Padding(
              padding: const EdgeInsets.all(15.0),
            ),
          ]),
      body: Center(),
      drawer: Drawer(
        child: Material(
          color: Colors.white,
          child: ListView(
            padding: padding,
            children: [
              buildHeader(
                urlimg: urlimg,
                name: name,
                email: email,
                onClicked: () {},
              ),
              menuItem(
                text: (getTranslated(context, 'home')),
                icon: Icons.home_outlined,
                onClicked: () => selectedItem(context, 0),
              ),
              SizedBox(height: 5),
              menuItem(
                text: (getTranslated(context, 'dashboard')),
                icon: Icons.dashboard,
                onClicked: () => selectedItem(context, 1),
              ),
              SizedBox(height: 5),
              menuItem(
                text: (getTranslated(context, 'inbox')),
                icon: Icons.inbox,
                onClicked: () => selectedItem(context, 2),
              ),
              SizedBox(height: 5),
              menuItem(
                text: (getTranslated(context, 'my_week')),
                icon: Icons.calendar_today,
                onClicked: () => selectedItem(context, 3),
              ),
              SizedBox(height: 5),
              menuItem(
                text: (getTranslated(context, 'notifications')),
                icon: Icons.notification_add,
                onClicked: () => selectedItem(context, 4),
              ),
              SizedBox(height: 5),
              menuItem(
                text: (getTranslated(context, 'my_team')),
                icon: Icons.group_outlined,
                onClicked: () => selectedItem(context, 5),
              ),
              SizedBox(height: 5),
              menuItem(
                text: (getTranslated(context, 'search')),
                icon: Icons.search,
                onClicked: () => selectedItem(context, 6),
              ),
              SizedBox(height: 5),
              Divider(
                color: Colors.black38,
              ),
              SizedBox(height: 5),

              menuItem(
                text: (getTranslated(context, 'invite_members')),
                icon: Icons.group_add,
                onClicked: () => selectedItem(context, 7),
              ),
              SizedBox(height: 5),
              menuItem(
                text: (getTranslated(context, 'help_center')),
                icon: Icons.device_unknown,
                onClicked: () => selectedItem(context, 8),
              ),
              SizedBox(height: 5),
              menuItem(
                text: (getTranslated(context, 'settings')),
                icon: Icons.settings,
                onClicked: () => selectedItem(context, 9),
              ),
              SizedBox(height: 5),
            
                  DropdownButton(
                    
                    onChanged: (Language language) {
                      _changeLanguage(language);
                    },
                          
                  
                   hint:  Text('language',style: TextStyle(color: Colors.black),),
                    items: Language.languageList()
                        .map<DropdownMenuItem<Language>>((lang) => DropdownMenuItem(
                          
                              value: lang,
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.spaceAround,
                                children: <Widget>[
                                  Text(
                                    lang.name,
                                  )
                                ],
                              ),
                            ))
                        .toList(),
                  ),
                  
              

              //....................................................................
              //SARA ADD THE BUTTONS HERE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
              //....................................................................

              ToggleButtons(
                children: [
                  Text(getTranslated(context, 'light')),
                  Text(getTranslated(context, 'dark')),
                ],
                isSelected: isSelected,
                onPressed: (int index) {
                  setState(
                    () {
                      for (int i = 0; i < isSelected.length; ++i) {
                        isSelected[i] = i == index;
                      }
                      Provider.of<ThemeModel>(context, listen: false)
                          .toggleTheme();
                    },
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget buildHeader({
    @required urlimg,
    @required name,
    @required email,
    @required VoidCallback onClicked,
  }) =>
      InkWell(
        onTap: onClicked,
        child: Container(
            padding: EdgeInsets.fromLTRB(0, 50, 0, 10),
            child: Row(
              children: [
                CircleAvatar(
                    radius: 35,
                    backgroundImage: AssetImage('assets/images/profile.jpg')),
                SizedBox(
                  width: 10,
                  height: 10,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      name,
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.black87,
                          fontWeight: FontWeight.bold),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      email,
                      style: TextStyle(fontSize: 15, color: Colors.black54),
                    ),
                  ],
                )
              ],
            )),
      );

  Widget menuItem({
    @required String text,
    @required IconData icon,
    VoidCallback onClicked,
  }) {
    final color = Colors.black;
    final hoverColor = Colors.black38;

    return ListTile(
      leading: Icon(
        icon,
        color: color,
      ),
      title: Text(
        text,
        style: TextStyle(color: color),
      ),
      hoverColor: hoverColor,
      onTap: onClicked,
    );
  }

  selectedItem(BuildContext context, int i) {
    Navigator.of(context).pop();
    switch (i) {
      case 0:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 1:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 2:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 3:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 4:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 5:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 6:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 7:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 8:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
      case 9:
        Navigator.of(context)
            .push(MaterialPageRoute(builder: (context) => HomePage()));
        break;
    }
  }

  void _changeLanguage(Language language) {
    Locale _temp;
    switch (language.languageCode) {
      case 'en':
        _temp = Locale(language.languageCode, 'US');
        break;
      case 'ar':
        _temp = Locale(language.languageCode, 'SA');
        break;
      default:
        _temp = Locale(language.languageCode, 'US');
    }
    MyApp.setLocale(context, _temp);
  }
}
